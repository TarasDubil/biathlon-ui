import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from "vuetify/es5/util/colors";
Vue.use(Vuetify);
export default new Vuetify({
  iconfont: "fa4",
  theme: {
    secondary: colors.pink.base,
    accent: colors.deepPurple.accent2,
    error: colors.red.accent4,
    info: colors.blue.lighten1,
    success: colors.green.accent4,
    warning: colors.amber.darken2
  },
  options: {
    themeVariations: ["primary", "secondary"]
  }
});
