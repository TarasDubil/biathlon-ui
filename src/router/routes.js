import AppToolbar from "@/layouts/App/ToolBar";
export const routes = [
  {
    path: "/",
    name: 'HomePage',
    components: {
      header: AppToolbar,
      default: resolve => require(["@/views/Pages/Home/Home"], resolve)
    }
  }
];